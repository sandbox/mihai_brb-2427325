CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

About

    This module allows to disable the site hash query by index.
    It also provides a views field handler to retrieve the raw data from SOLR document. (Note: This requires a patch.)

Scenario

We have one Solr indexing server core configured on two sites.
A Drupal Site A is indexing nodes on "default_node_index".
Another Drupal Site B is configured to read nodes from "default_node_index". This Site B instance of Drupal does not have the indexed fields present as in Site A.

In order for data to be pulled in Site B:

    the site hash filter has to removed from query
    you need to enable on SOLR server the option "Retrieve result data from Solr"
    one can use the "Multisite SOLR raw data" field and configure the solr index key


REQUIREMENTS
------------
This module requires the following modules:

* Search Api Solr (https://drupal.org/project/search_api_solr)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

* Configure.

<?php

/**
 * @file
 * Views integration for Search Api Solr Multisite.
 */

/**
 * Implements hook_views_data_alter().
 */
function search_api_solr_multisite_views_data_alter(array &$data) {
  $disabled = variable_get('search_api_solr_multisite', array());
  foreach ($disabled as $index_name) {
    if ($index = search_api_index_load($index_name)) {
      // Fill in base data.
      $key = 'search_api_index_' . $index->machine_name;
      $table = &$data[$key];
      $table['search_api_solr_multisite'] = array(
        'field' => array(
          'title' => t('Multisite Solr raw data'),
          'help' => t('Search API Solr multisite raw document data.'),
          'handler' => 'search_api_solr_multisite',
        ),
      );
    }
  }
}

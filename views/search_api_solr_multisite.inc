<?php

/**
 * @file
 * Search Api SOLR Multisite views field handler.
 */

/**
 * Field handler for retreiving raw data from Solr response document.
 */
class search_api_solr_multisite extends views_handler_field {

  /**
   * An array to store documents retreived from SOLR.
   *
   * @var array
   */
  public $docs = array();

  /**
   * {@inheritdoc}
   */
  public function construct() {
    parent::construct();
  }

  /**
   * Sets our own default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    // The field name, as stored in SOLR.
    $options['field_name'] = array('default' => '');
    // Rendering format for field value to pass trough check_plain.
    $options['field_format'] = array('default' => 'plain_text');
    // HTML ul, ol, or just a simple separator.
    $options['field_type'] = array('default' => 'separator');
    // Separator in case of "separator" field_type.
    $options['field_type_separator'] = array('default' => ', ');
    // How many items to show. Valid options are "all" or a number.
    $options['field_delta'] = array('default' => 'all');
    // Offset for showing items.
    $options['field_delta_offset'] = array('default' => '0');

    // @TODO: Maybe add the reverse option, as the views does.

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Provide user-input element for adding the index key, as used in Solr.
    $form['field_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Add the index key.'),
      '#default_value' => $this->options['field_name'],
      '#required' => TRUE,
    );
    // Provide a format for rendering value(s).
    $formats = filter_formats();
    $format_options = array(FALSE => 'Raw (none)');
    foreach ($formats as $format) {
      $format_options[$format->format] = $format->name;
    }
    $form['field_format'] = array(
      '#type' => 'radios',
      '#title' => t('Select a text format to pass the value through.'),
      '#default_value' => $this->options['field_format'],
      '#options' => $format_options,
      '#required' => TRUE,
    );
    // Settings for multiple values.
    $form['multiple_field_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Multiple field settings'),
      '#description' => t('Display all values in the same row.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['field_type'] = array(
      '#type' => 'radios',
      '#title' => t('Display type'),
      '#options' => array(
        'ul' => t('Unordered list'),
        'ol' => t('Ordered list'),
        'separator' => t('Simple separator'),
      ),
      '#default_value' => $this->options['field_type'],
      '#fieldset' => 'multiple_field_settings',
    );
    $form['field_type_separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#default_value' => $this->options['field_type_separator'],
      '#states' => array(
        'visible' => array(
          ':input[name="options[field_type]"]' => array('value' => 'separator'),
        ),
      ),
      '#fieldset' => 'multiple_field_settings',
    );
    $form['field_delta'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#required' => TRUE,
      '#element_validate' => array('search_api_solr_multisite_field_delta_validate'),
      '#field_prefix' => t('Display'),
      '#field_suffix' => t('value(s)'),
      '#prefix' => '<div class="container-inline">',
      '#default_value' => $this->options['field_delta'],
      '#fieldset' => 'multiple_field_settings',
    );
    $form['field_delta_offset'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#field_prefix' => t('starting from'),
      '#default_value' => $this->options['field_delta_offset'],
      '#description' => t('(first item is 0)'),
      '#fieldset' => 'multiple_field_settings',
      '#suffix' => '</div>',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // No query needs to be ran, as we retreive raw documents from the server.
  }

  /**
   * Helper, make sure that docs are keyed by solr id and stored in $docs.
   */
  public function docs() {
    // Get raw resoults and cache then inside this handler.
    $raw_results = $this->view->query->getSearchApiResults();
    if (!isset($raw_results['search_api_solr_response']->response->docs)) {
      return;
    }
    foreach ($raw_results['search_api_solr_response']->response->docs as $doc) {
      $this->docs[$doc->id] = $doc;
    }

    return $this->docs;
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    // We've passed over the id via "_id" key. Validate search api id.
    if (!isset($values->_entity_properties['_id'])) {
      return;
    }
    $id = $values->_entity_properties['_id'];

    // Populate $docs keyed by search_api_id.
    if (empty($this->docs)) {
      $this->docs();
    }

    // Get the value we are looking for.
    if (!isset($this->docs[$id])) {
      return NULL;
    }
    $doc = $this->docs[$id];

    // Validate the field name we are to display data for.
    $options = $this->options;
    if (!isset($options['field_name']) || !isset($doc->{$options['field_name']})) {
      return NULL;
    }
    $field = $options['field_name'];

    // Check if we have any values.
    if (empty($doc->$field)) {
      return NULL;
    }

    // Check the user selected format.
    $format = isset($options['field_format']) && !empty($options['field_format']) ? $options['field_format'] : FALSE;
    // Handle multi-value field values.
    if (is_array($doc->$field)) {
      if ($options['field_delta'] == 'all') {
        $length = count($doc->$field);
      }
      else {
        $length = $options['field_delta'];
      }
      $offset = $options['field_delta_offset'];
      $values = array_slice($doc->$field, $offset, $length);

      return $this->renderMultiple($values, $format);
    }
    // Handle single-value field values.
    else {
      return $this->renderSingle($doc->$field, $format);
    }
  }

  /**
   * Helper: filter item value.
   */
  public function formatFilter($values, $format = FALSE) {
    if (!$format) {
      return $values;
    }
    if (is_array($values)) {
      foreach ($values as &$value) {
        $value = check_markup($value, $format);
      }
    }
    else {
      $values = check_markup($values, $format);
    }
    return $values;
  }

  /**
   * Render multiple values.
   */
  public function renderMultiple($items, $format) {
    if ($this->options['field_type'] == 'separator') {
      return implode(filter_xss_admin($this->options['field_type_separator']), $items);
    }
    else {
      $items = $this->formatFilter($items, $format);
      return theme('item_list', array(
        'items' => $items,
        'title' => NULL,
        'type' => $this->options['field_type'],
      ));
    }
  }

  /**
   * Render single-value items.
   */
  public function renderSingle($values, $format) {
    return $this->formatFilter($values, $format);
  }

}

/**
 * Element validate for "field_delta" options form element.
 */
function search_api_solr_multisite_field_delta_validate($element, &$form_state) {
  // Valid options are "all" string or a positive number.
  if (!empty($element['#value']) && $element['#value'] == 'all') {
    return;
  }
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value <= 0)) {
    form_error($element, t('This option must contain a valid value: a number or "all".'));
  }
}
